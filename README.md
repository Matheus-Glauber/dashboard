# Dashboard

Esse projeto foi gerado via [Angular CLI](https://github.com/angular/angular-cli) versão 8.3.20.

## Baixando pela primeira vez o projeto

No diretório selecionado por você, digite `git clone https://gitlab.com/Matheus-Glauber/dashboard.git`, depois digite o comando `npm i` ou `npm install` para instalar as dependências do projeto.

## Rodando o Servidor

Execute `ng serve` para rodar no servidor local. Navegue até o `http://localhost:4200/`. Sempre que um arquivo for alterado e salvo, o servidor irá rodar automaticamente.

## Criando componentes, diretivas, pipes, serviços, interfaces, enums, etc

Execute `ng generate component component-name` para gerar um novo componente. Se quiser criar outro tipo de arquivo execute `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Execute `ng build` para "buildar" o projeto. Os artefatos "buildados" serão salvos na pasta `dist/` do seu diretório. Use o `--prod` para por seu "build" em produção.

## Rodando os testes unitários

Execute `ng test` para executar os testes unitários via [Karma](https://karma-runner.github.io).

## Rodando testes de ponta a ponta

Execute `ng e2e` para executar os testes de ponta a ponta via [Protractor](http://www.protractortest.org/).

## Ajuda

Para obter ajuda sopbre o Angular CLI use `ng help` ou consulte o seguinte link [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
