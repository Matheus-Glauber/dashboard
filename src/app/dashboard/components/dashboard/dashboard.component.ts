import { Component, OnInit } from "@angular/core";
import { DadosService } from "../../services/dados.service";

declare var google: any;

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  private dados: any;

  constructor(private dadosService: DadosService) {}

  ngOnInit() {
    this.dadosService.obterDados().subscribe(dados => {
      this.dados = dados;
      this.init();
    });
  }

  /**
   * Inicializa a API de gráficos com delay de 1 segundo, o que permite a integração da API com o Angular.
   *
   * @returns void
   */
  init(): void {
    if (typeof google !== "undefined") {
      google.charts.load("current", { packages: ["corechart"] });
      setTimeout(() => {
        google.charts.setOnLoadCallback(this.exibirGraficos());
      }, 1000);
    }
  }

  /**
   * Método chamado assim que a API de gráficos é inicializada. Responsável por chamar os métodos geradores dos gráficos.
   *
   * @returns void
   */
  exibirGraficos(): void {
    this.exibirPieChart();
    this.exibir3dPieChart();
    this.exibirBarChart();
    this.exibirLineChart();
    this.exibirColumnChart();
    this.exibirDonutChart();
  }

  /**
   * Exibe o gráfico Donut Chart.
   *
   * @returns void
   */
  exibirDonutChart() {
    const el = document.getElementById("donut_chart");
    const chart = new google.visualization.PieChart(el);
    const opcoes = this.obterOpcoes();

    opcoes["pieHole"] = 0.4;
    chart.draw(this.obterDataTable(), opcoes);
  }

  /**
   * Exibe o gráfico Column Chart.
   *
   * @returns void
   */
  exibirColumnChart() {
    const el = document.getElementById("column_chart");
    const chart = new google.visualization.ColumnChart(el);

    chart.draw(this.obterDataTable(), this.obterOpcoes());
  }

  /**
   * Exibe o gráfico Line Chart.
   *
   * @returns void
   */
  exibirLineChart() {
    const el = document.getElementById("line_chart");
    const chart = new google.visualization.LineChart(el);

    chart.draw(this.obterDataTable(), this.obterOpcoes());
  }

  /**
   * Exibe o gráfico Pie Chart.
   *
   * @returns void
   */
  exibirPieChart(): void {
    const el = document.getElementById("pie_chart");
    const chart = new google.visualization.PieChart(el);

    chart.draw(this.obterDataTable(), this.obterOpcoes());
  }

  /**
   * Exibe o gráfico Pie Chart 3D.
   *
   * @returns void
   */
  exibir3dPieChart(): void {
    const el = document.getElementById("3d_pie_chart");
    const chart = new google.visualization.PieChart(el);
    const opcoes = this.obterOpcoes();

    opcoes["is3D"] = true;
    chart.draw(this.obterDataTable(), opcoes);
  }

  /**
   * Exibe o gráfico Donut Chart.
   *
   * @returns void
   */
  exibirBarChart(): void {
    const el = document.getElementById("bar_chart");
    const chart = new google.visualization.BarChart(el);

    chart.draw(this.obterDataTable(), this.obterOpcoes());
  }

  /**
   * Retorna as opções do gráfico, que incluem o título e tamanho do gráfico.
   *
   * @returns any
   */
  obterOpcoes(): any {
    return {
      title: "Quantidade de cadastros primeiro semestre",
      width: 400,
      height: 300
    };
  }

  /**
   * Cria e retorna um objeto DataTable da API de gráficos, responsável por definir os dados do gráfico.
   *
   * @return data
   */
  obterDataTable(): any {
    const data = new google.visualization.DataTable();

    data.addColumn("string", "Mês");
    data.addColumn("number", "Quantidade");
    data.addRows(this.dados);

    return data;
  }
}
